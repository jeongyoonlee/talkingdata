# -*- coding: utf-8 -*-
"""
Created on Wed Aug 31 22:02:19 2016

@author: Luca
"""
from random import shuffle

filename = 'h4'

with open('sample_submission.csv','rb') as S:
    header=S.readline()
    with open(filename+'.tst.sps','rb') as R:
        with open(filename+'.tst.vw','wb') as W:
            for n, (row,sample) in enumerate(zip(R,S)):
                sequence = row.split(' ')
                target = str(int(sequence[0])+1) + " '"+sample.split(',')[0]+' |F '
                features = ' '.join(sequence[1:])
                W.write(target + features)

observed = set()
with open(filename+'.trn.sps','rb') as R:
    with open(filename+'.trn.vw','wb') as W:
        for n, row in enumerate(R):
            sequence = row.split(' ')
            target_value = str(int(sequence[0])+1)
            observed.add(target_value)
            target = target_value + " '"+str(n+1)+' |F '
            features = ' '.join(sequence[1:])
            W.write(target + features)

print 'observed classes:',sorted(map(int,observed))

with open(filename+'.trn.vw','rb') as R:
    original = R.readlines()
    shuffle(original)
    with open(filename+'.shuffle.trn.vw','wb') as W:
        for element in original:
            W.write(element)
    